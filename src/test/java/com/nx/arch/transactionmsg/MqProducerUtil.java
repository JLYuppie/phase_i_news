package com.nx.arch.transactionmsg;

import com.alibaba.rocketmq.client.exception.MQClientException;
import com.alibaba.rocketmq.client.producer.DefaultMQProducer;

public class MqProducerUtil {
    
    public static DefaultMQProducer geyProducer() {
        // producer group
        DefaultMQProducer producer = new DefaultMQProducer("hello_test");
        // mq 地址
        producer.setNamesrvAddr("192.168.188.63:9876;192.168.188.64:9876");
        try {
            producer.start();
        } catch (MQClientException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return producer;
    }
}
