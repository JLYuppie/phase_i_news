package com.nx.arch.transactionmsg;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.junit.Test;

import com.alibaba.druid.pool.DruidDataSource;

import io.shardingjdbc.core.api.ShardingDataSourceFactory;
import io.shardingjdbc.core.api.config.ShardingRuleConfiguration;

public class ShardingJdbcTest {
    
    private static String mysqlUrl = "jdbc:mysql://127.0.0.1:13306/arch_config?zeroDateTimeBehavior=convertToNull";
    
    @Test
    public void testShardConnection() {
        // 配置真实数据源
        Map<String, DataSource> dataSourceMap = new HashMap<>(10);
        
        // 配置第一个数据源
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(mysqlUrl);
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        dataSource.setMinIdle(5);
        dataSource.setInitialSize(1);
        dataSource.setMaxActive(5);
        dataSourceMap.put("ds0", dataSource);
        try {
            dataSource.init();
        } catch (SQLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
        // shardingRuleConfig.getTableRuleConfigs().add(orderTableRuleConfig);
        
        try {
            // DataSource dataSourceSharding = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfig, new ConcurrentHashMap(), new Properties());
            DataSource dataSourceSharding = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfig, new ConcurrentHashMap<String, Object>(10), new Properties());
            
            /*
             * java.sql.Connection conn = dataSourceSharding.getConnection();
             * 
             * // conn.getMetaData();
             * 
             * conn.setAutoCommit(false);
             * 
             * java.sql.Statement statement = conn.createStatement(); //ResultSet result = statement.executeQuery("select count(*) from mq_messages;"); //System.out.println("result="+result.getString(1)); String str =
             * "insert into mq_messages(content,topic,tag) value('hellocontent','hello_topic','taga')"; statement.execute(str); // statement conn.commit();
             * 
             * statement.close(); conn.close();
             */
            
            for (int i = 0; i < 60; i++) {
                java.sql.Connection con = dataSourceSharding.getConnection();
                String url = "";
                url = con.getMetaData().getURL();
                
                System.out.println("index22=" + i + " " + con.getAutoCommit() + " =" + con + " url=" + url);
            }
            for (;;) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
